![](https://media.istockphoto.com/vectors/panoramic-view-bouquet-of-carnation-schabaud-spring-blossom-border-vector-id1141636140?k=6&m=1141636140&s=170667a&w=0&h=0WD4iJgMck9ehLDyu9cGdoNjI1MG425GPA8wcdxSuxs=)
___
### ***DAME LA MANO***
___
####**de [Gabriela Mistral](https://es.wikipedia.org/wiki/Gabriela_Mistral)**
___
>Dame la mano y danzaremos;
dame la mano y me amarás.
Como una sola flor seremos,
como una flor, y nada más...

>El mismo verso cantaremos,
al mismo paso bailarás.
Como una espiga ondularemos,
como una espiga, y nada más.

>Te llamas Rosa y yo Esperanza;
pero tu nombre olvidarás,
porque seremos una danza
en la colina y nada más...
___
